/**
 * File: /src/hooks/useCalculateMoti.ts
 * Project: @native-theme-ui/core
 * File Created: 02-09-2022 06:18:49
 * Author: Clay Risser
 * -----
 * Last Modified: 04-09-2022 07:54:21
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { SxProp, useSx } from "dripsy";
import { MotiProps } from "moti";

export interface CalculateMotiProps {
  animate?: Pick<MotiProps, "animate">;
  animateSx?: SxProp;
  exit?: Pick<MotiProps, "exit">;
  exitSx?: SxProp;
  from?: Pick<MotiProps, "from">;
  fromSx?: SxProp;
  themeKey?: string;
}

export function useCalculateMoti(props: any, themeKey?: any, isMoti = true) {
  const sx = useSx();
  const motiProps = props as CalculateMotiProps;
  const animate = (motiProps.animate ||
    (isMoti && motiProps.animateSx
      ? sx(motiProps.animateSx, { themeKey: motiProps.themeKey || themeKey })
      : // eslint-disable-next-line @typescript-eslint/no-explicit-any
        {})) as any;
  const from = (motiProps.from ||
    (isMoti && motiProps.fromSx
      ? sx(motiProps.fromSx, { themeKey: motiProps.themeKey || themeKey })
      : // eslint-disable-next-line @typescript-eslint/no-explicit-any
        {})) as any;
  const exit = (motiProps.exit ||
    (isMoti && motiProps.exitSx
      ? sx(motiProps.exitSx, { themeKey: motiProps.themeKey || themeKey })
      : // eslint-disable-next-line @typescript-eslint/no-explicit-any
        {})) as any;
  return { from, animate, exit };
}
