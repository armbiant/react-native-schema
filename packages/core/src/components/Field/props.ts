/**
 * File: /src/components/Field/props.ts
 * Project: @native-theme-ui/core
 * File Created: 05-07-2022 06:24:30
 * Author: Clay Risser
 * -----
 * Last Modified: 24-08-2022 09:38:53
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Sx } from "dripsy";
import { createUseSplitProps } from "../../util";
import { InputProps } from "../Input";
import { splitPropsGroups } from "../../util";

export type BasePropsBucket = InputProps;

export type LabelPropsBucket = {
  label: string;
};

export const useSplitProps = createUseSplitProps<
  FieldProps,
  {
    baseProps: BasePropsBucket;
    labelProps: LabelPropsBucket;
  },
  {
    baseSx: Sx;
    parentSx: Sx;
    textSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
    parentStyle: Record<string, unknown>;
    textStyle: Record<string, unknown>;
  }
>(
  {
    labelProps: ["label"],
  },
  {
    parentSx: splitPropsGroups.sx.margin,
    textSx: /^(font)|(text)|(color)/g,
  }
);

export type FieldProps = BasePropsBucket & LabelPropsBucket;
