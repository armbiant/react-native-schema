/**
 * File: /src/components/Box/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 07-09-2022 14:55:06
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { FC } from "react";
import { MotiPressable } from "moti/interactions";
import { MotiView } from "moti";
import { Sx, styled, useSx } from "dripsy";
import { View as RNView } from "react-native";
import {
  BackgroundColorProvider,
  useAutoContrast,
} from "@risserlabs/auto-contrast";
import { BoxProps, useSplitProps } from "./props";
import { Text } from "../Text";
import { isText } from "../../util";
import { useCalculateMoti } from "../../hooks";

const themeKey = "variants";

const defaultOuterStyle: Sx = {
  boxSizing: "border-box",
  cursor: "auto",
  flexShrink: 1,
};

const defaultInnerStyle: Sx = {
  display: "flex",
  flexShrink: 1,
  flexDirection: "column",
};

const StyledView = styled(RNView, {
  themeKey,
})({ ...defaultInnerStyle, ...defaultOuterStyle });

const StyledMotiPressable = styled(MotiPressable, {
  themeKey,
})(defaultInnerStyle);

const StyledMotiView = styled(MotiView, {
  themeKey,
})(defaultInnerStyle);

export const Box: FC<BoxProps> = (props: BoxProps) => {
  const sx = useAutoContrast(props, {}, themeKey);
  const calcSx = useSx();
  const {
    baseProps,
    baseStyle,
    baseSx,
    innerStyle,
    innerSx,
    motiPressableOuterStyle,
    motiPressableOuterSx,
    motiProps,
    pressableProps,
    sharedStyle,
    sharedSx,
    textProps,
    textStyle,
    textSx,
  } = useSplitProps(props, sx);
  const isPressable = !!Object.keys(pressableProps).length;
  const isMoti = !!Object.keys(motiProps).length;
  const { animate, from, exit } = useCalculateMoti(props, themeKey, isMoti);

  const children = isText(props.children) ? (
    <Text {...textProps} sx={textSx} style={textStyle}>
      {props.children}
    </Text>
  ) : (
    props.children
  );

  const renderChildren = () => (
    <BackgroundColorProvider {...props} themeKey={themeKey}>
      {children}
    </BackgroundColorProvider>
  );

  if (isPressable) {
    if (isMoti) {
      const outerStyle = {
        ...defaultOuterStyle,
        ...calcSx(motiPressableOuterSx || {}, { themeKey }),
        ...calcSx(sharedSx || {}, { themeKey }),
        ...motiPressableOuterStyle,
        ...sharedStyle,
      };
      return (
        <StyledMotiPressable
          {...(pressableProps as any)}
          containerStyle={outerStyle}
          sx={sharedSx}
          style={{
            alignItems: "center",
            display: "flex",
            height: "100%",
            justifyContent: "center",
            margin: 0,
            overflow: "visible",
            padding: 0,
            width: "100%",
            ...sharedStyle,
          }}
        >
          <StyledMotiView
            {...(baseProps as any)}
            {...(motiProps as any)}
            animate={animate}
            exit={exit}
            from={from}
            style={{ ...baseStyle, ...sharedStyle, ...innerStyle }}
            sx={{ ...baseSx, ...sharedSx, ...innerSx }}
          >
            {renderChildren()}
          </StyledMotiView>
        </StyledMotiPressable>
      );
    }
    const outerStyle = {
      ...defaultOuterStyle,
      ...calcSx(baseSx || {}, { themeKey }),
      ...calcSx(motiPressableOuterSx || {}, { themeKey }),
      ...calcSx(sharedSx || {}, { themeKey }),
      ...motiPressableOuterStyle,
      ...baseStyle,
      ...sharedStyle,
    };
    return (
      <StyledMotiPressable
        {...(baseProps as any)}
        {...(pressableProps as any)}
        containerStyle={outerStyle}
        style={{ ...sharedStyle, ...innerStyle }}
        sx={{ ...sharedSx, ...innerSx }}
      >
        {renderChildren()}
      </StyledMotiPressable>
    );
  }
  if (isMoti) {
    return (
      <StyledMotiView
        {...(baseProps as any)}
        {...(motiProps as any)}
        animate={animate}
        exit={exit}
        from={from}
        style={{
          ...baseStyle,
          ...innerStyle,
          ...motiPressableOuterStyle,
          ...sharedStyle,
        }}
        sx={{
          ...baseSx,
          ...innerSx,
          ...motiPressableOuterSx,
          ...sharedSx,
        }}
      >
        {renderChildren()}
      </StyledMotiView>
    );
  }
  return (
    <StyledView
      {...(baseProps as any)}
      style={{
        ...baseStyle,
        ...innerStyle,
        ...motiPressableOuterStyle,
        ...sharedStyle,
      }}
      sx={{
        ...baseSx,
        ...innerSx,
        ...motiPressableOuterSx,
        ...sharedSx,
      }}
    >
      {renderChildren()}
    </StyledView>
  );
};

export type { BoxProps };
