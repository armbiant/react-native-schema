/**
 * File: /src/components/Message/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 23-06-2022 18:32:39
 * Author: Ajithkrm6
 * -----
 * Last Modified: 26-08-2022 06:07:03
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ComponentProps } from "react";
import { styled, Sx } from "dripsy";
import { useAutoContrast } from "@risserlabs/auto-contrast";
import { Box } from "../Box";

const themeKey = "messages";

const defaultStyle: Sx = {
  bg: "highlight",
  borderLeftColor: "primary",
  borderLeftStyle: "solid",
  borderLeftWidth: (t) => t.space && Number(t.space[1]),
  borderRadius: 4,
  padding: 3,
  paddingLeft: (t) => t.space && Number(t.space[3]) - Number(t.space[1]),
};

const StyledBox = styled(Box, {
  themeKey,
})(defaultStyle);

export type MessageProps = ComponentProps<typeof StyledBox>;

export const Message: FC<MessageProps> = (props: MessageProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  return <StyledBox {...props} sx={sx} />;
};
