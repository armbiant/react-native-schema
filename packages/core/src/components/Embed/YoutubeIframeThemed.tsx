/**
 * File: /src/components/Embed/YoutubeIframeThemed.tsx
 * Project: @native-theme-ui/core
 * File Created: 18-07-2022 08:05:40
 * Author: Clay Risser
 * -----
 * Last Modified: 24-08-2022 08:40:30
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from "react";
import YoutubeIframe, { YoutubeIframeProps } from "react-native-youtube-iframe";
import { StyleSheet } from "react-native";
import { createThemedComponent, SxProp } from "dripsy";

type YoutubeIframeStyledProps = Omit<
  YoutubeIframeProps,
  "width" | "height" | "webViewStyle"
> & {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  style?: any;
};

const YoutubeIframeStyled: FC<YoutubeIframeStyledProps> = (
  props: YoutubeIframeStyledProps
) => {
  const style = StyleSheet.flatten(props.style || {});
  const webViewStyle = { ...style };
  delete webViewStyle.width;
  delete webViewStyle.height;
  return (
    <YoutubeIframe
      {...props}
      height={style?.height}
      width={style?.width}
      webViewStyle={webViewStyle}
    />
  );
};

export type YoutubeIframeThemedProps = YoutubeIframeStyledProps & {
  sx?: SxProp;
};

const YoutubeIframeThemed = createThemedComponent(YoutubeIframeStyled);

export default YoutubeIframeThemed;
