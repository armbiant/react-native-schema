/**
 * File: /src/components/Button/props.ts
 * Project: @native-theme-ui/core
 * File Created: 05-07-2022 07:43:20
 * Author: Clay Risser
 * -----
 * Last Modified: 10-09-2022 12:35:18
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MotiPressableProps } from "moti/interactions";
import { Sx, SxProp } from "dripsy";
import { DripsyStyledProps } from "../../types";
import { createUseSplitProps, splitPropsGroups } from "../../util";
import { TextProps } from "../Text";

export type BasePropsBucket = DripsyStyledProps<MotiPressableProps> & {
  hidden?: boolean;
};

export type TextPropsBucket = TextProps;

export type MotiPropsBucket = {
  animateSx?: SxProp;
  exitSx?: SxProp;
  fromSx?: SxProp;
};

export const useSplitProps = createUseSplitProps<
  ButtonProps,
  {
    baseProps: BasePropsBucket;
    motiProps: MotiPropsBucket;
    textProps: TextPropsBucket;
  },
  {
    baseSx: Sx;
    innerSx: Sx;
    motiPressableOuterSx: Sx;
    sharedSx: Sx;
    textSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
    innerStyle: Record<string, unknown>;
    motiPressableOuterStyle: Record<string, unknown>;
    sharedStyle: Record<string, unknown>;
    textStyle: Record<string, unknown>;
  }
>(
  {
    textProps: "autoContrast",
    motiProps: splitPropsGroups.props.moti,
  },
  {
    innerSx: splitPropsGroups.sx.inner,
    motiPressableOuterSx: splitPropsGroups.sx.outer,
    sharedSx: ["borderRadius"],
    textSx: splitPropsGroups.sx.text,
  }
);

export type ButtonProps = BasePropsBucket & TextPropsBucket & MotiPropsBucket;
