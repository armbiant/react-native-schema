/**
 * File: /src/components/Image/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 19-10-2022 07:49:18
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { XMLParser } from "fast-xml-parser";
import React, { FC, ComponentProps, useState, useEffect } from "react";
import { Sx, styled } from "dripsy";
import { SvgUri } from "react-native-svg";
import {
  Image as RNImage,
  ImageResizeMode,
  ImageSourcePropType,
  ImageStyle,
  ImageURISource,
  Platform,
  StyleSheet,
} from "react-native";
import { useCalculatedSx } from "@risserlabs/auto-contrast";
import { Box } from "../Box";
import { ImageProps, useSplitProps } from "./props";
import { Asset } from "expo-asset";

const logger = console;

const defaultStyle: Sx = {
  height: "auto",
  maxWidth: "100%",
  width: "100%",
};

const StyledImage = styled(RNImage, { themeKey: "images" })(defaultStyle);

const StyledSvgUri = styled(SvgUri, { themeKey: "images" })(defaultStyle);

const StyledOuterBox = styled(Box)({
  position: "relative",
  overflow: "hidden",
});

const parser = new XMLParser({
  ignoreAttributes: false,
  attributeNamePrefix: "@_",
  allowBooleanAttributes: true,
});

export const Image: FC<ImageProps> = (props: ImageProps) => {
  const [imageRatio, setImageRatio] = useState<number>();
  const sx = useCalculatedSx({ ...props.sx });
  const style = StyleSheet.flatten(props.style || {}) as ImageStyle;
  style.resizeMode = getResizeMode(
    sx,
    (style as { objectFit?: string }).objectFit,
    style.resizeMode
  );
  delete (style as { objectFit?: string }).objectFit;
  delete sx.objectFit;
  const {
    baseProps,
    baseStyle,
    baseSx,
    innerStyle,
    innerSx,
    motiPressableProps,
  } = useSplitProps(props, sx);
  const isMotiPressable = !!Object.keys(motiPressableProps).length;
  let { svg } = props;

  const styledImageProps = { ...baseProps } as ComponentProps<
    typeof StyledImage
  > & {
    src?: string | ImageSourcePropType;
  };
  let source = props.src as ImageSourcePropType;
  if (typeof props.src === "string") {
    source = { uri: props.src };
  } else if ((props.src as Asset)?.localUri && (props.src as Asset)?.type) {
    source = {
      uri: (props.src as Asset).uri,
    };
    if ((props.src as Asset).type === "svg" && typeof svg === "undefined") {
      svg = true;
    }
  }
  delete styledImageProps.src;
  const uri =
    typeof source === "number" ? source : (source as ImageURISource)?.uri;

  const calculateHeight =
    style.height === "auto" ||
    (typeof style.height === "undefined" &&
      (typeof sx.height === "undefined" || sx.height === "auto"));

  useEffect(() => {
    if (typeof uri !== "number" && uri && !imageRatio && calculateHeight) {
      if (svg) {
        if (uri) {
          (async () => {
            const res = await fetch(uri);
            try {
              const xml = parser.parse(await res.text());
              const width = parseInt(xml?.svg?.["@_width"]);
              const height = parseInt(xml?.svg?.["@_height"]);
              if (
                typeof width === "number" &&
                !Number.isNaN(width) &&
                typeof height === "number" &&
                !Number.isNaN(height)
              ) {
                setImageRatio(width / height);
              }
            } catch (err) {
              logger.error(err);
            }
          })();
        }
      } else {
        RNImage.getSize(uri, (width: number, height: number) => {
          setImageRatio(width / height);
        });
      }
    }
  }, [uri, imageRatio, calculateHeight, props.src]);

  function renderImage(props: any) {
    if (!props.src || !uri) <Box {...props} />;
    if (svg && Platform.OS !== "web") {
      return (
        <StyledSvgUri
          {...props}
          onLayout={(e) => {
            console.log("layoutEvent", e.nativeEvent);
          }}
          height={style.height ?? sx.height}
          uri={uri}
          width={style.width ?? sx.width}
        />
      );
    }
    return <StyledImage {...props} source={source} />;
  }

  if (calculateHeight && imageRatio) {
    return (
      <StyledOuterBox {...motiPressableProps} sx={baseSx} style={baseStyle}>
        <Box
          style={{
            height: 0,
            paddingBottom: `${100 / imageRatio}%`,
            width: "100%",
          }}
        />
        {renderImage({
          ...styledImageProps,
          sx: innerSx,
          style: {
            ...innerStyle,
            position: "absolute",
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
          },
        })}
      </StyledOuterBox>
    );
  }

  if (isMotiPressable) {
    return (
      <StyledOuterBox
        {...motiPressableProps}
        sx={baseSx}
        style={{ ...baseStyle }}
      >
        {renderImage({
          ...styledImageProps,
          sx: innerSx,
          style: {
            ...innerStyle,
            overflow: "visible",
            width: "100%",
            height: "100%",
          },
        })}
      </StyledOuterBox>
    );
  }

  return renderImage({
    ...styledImageProps,
    sx,
    style,
  });
};

function getResizeMode(
  sx: Sx,
  objectFit?: string,
  resizeMode?: ImageResizeMode
): ImageResizeMode {
  if (resizeMode) return resizeMode;
  if (objectFit === "fill") return "stretch";
  if (objectFit === "cover" || objectFit === "contain") return objectFit;
  if (sx.objectFit === "fill") return "stretch";
  if (sx.objectFit === "cover" || sx.objectFit === "contain") {
    return sx.objectFit;
  }
  return "contain";
}

export type { ImageProps };
