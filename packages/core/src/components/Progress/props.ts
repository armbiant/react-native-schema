/**
 * File: /src/components/Progress/props.ts
 * Project: @native-theme-ui/core
 * File Created: 05-07-2022 06:24:30
 * Author: Clay Risser
 * -----
 * Last Modified: 26-08-2022 07:33:56
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Sx } from "dripsy";
import { createUseSplitProps, splitPropsGroups } from "../../util";
import { StyledBoxProps } from "./index";

export type BasePropsBucket = StyledBoxProps & {
  max?: number;
  value?: number;
};

export const useSplitProps = createUseSplitProps<
  ProgressProps,
  {
    baseProps: BasePropsBucket;
  },
  {
    baseSx: Sx;
    nullSx: Sx;
    colorSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
    nullStyle: Record<string, unknown>;
    colorStyle: Record<string, unknown>;
  }
>(
  {},
  {
    nullSx: splitPropsGroups.sx.padding,
    colorSx: ["color"],
  }
);

export type ProgressProps = Omit<BasePropsBucket, "children">;
