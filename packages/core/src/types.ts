/**
 * File: /src/types.ts
 * Project: @native-theme-ui/core
 * File Created: 02-09-2022 11:22:24
 * Author: Clay Risser
 * -----
 * Last Modified: 02-09-2022 11:29:25
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { StyledProps, DripsyFinalTheme } from "dripsy";
import {
  PropsWithoutRef,
  RefAttributes,
  Component,
  PropsWithRef,
  ReactNode,
} from "react";

export type DripsyStyledProps<
  Props,
  ThemeKey extends keyof DripsyFinalTheme = keyof DripsyFinalTheme
> = StyledProps<ThemeKey> &
  (
    | (PropsWithoutRef<Props> & RefAttributes<Component<Props, any, any>>)
    | PropsWithRef<
        Props & {
          children?: ReactNode;
        }
      >
  );
